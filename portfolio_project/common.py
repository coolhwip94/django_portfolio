import configparser
config = configparser.ConfigParser(interpolation=None)
config.read_file(open('app_setup.cfg'))

secret_key = config['django']['secret_key']