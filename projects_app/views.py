from django.shortcuts import render
from django.http import HttpResponse
from projects_app.models import Project

# Create your views here.


def all_projects(request):
    # query the database
    projects = Project.objects.all()
    return render(request, 'projects_app/all_projects.html', {'projects': projects})


def project_detail(request, pk):
    project = Project.objects.get(pk=pk)
    return render(request, 'projects_app/details.html', {'project': project})
