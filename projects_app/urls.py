from django.urls import path
from projects_app import views
app_name = "projects_app"  # need to be present for django to identify app
urlpatterns = [
    path('', views.all_projects, name="all_projects"),
    path('<int:pk>', views.project_detail, name="project_detail"),
]
